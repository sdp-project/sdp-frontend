import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.scss']
})
export class ViewOrderComponent implements OnInit {
  orderDetail: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private orderService: OrderService) { }

  ngOnInit(): void {
    console.log(this.data);
    this.orderService.getOrderDetails(this.data.orderID).subscribe((result) => {
      this.orderDetail = result.data;
    });
  }

}
