import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/service/auth.service';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  getOrderList(): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.get<any>(`${environment.apiUrl}/order/${userID}`);
  }

  getOrderDetails(orderID): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.get<any>(`${environment.apiUrl}/order/${userID}/${orderID}`);
  }
}
