import { Component, OnInit } from '@angular/core';
import { OrderService } from './order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  orderList;
  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.orderService.getOrderList().subscribe((result) => {
      this.orderList = result.data;
    });
  }

}
