import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/core/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AddCardService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  createCard(value: any): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.post<any>(`${environment.apiUrl}/card/${userID}`, { ...value });

  }
}
