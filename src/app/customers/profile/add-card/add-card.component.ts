import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AddCardService } from './add-card.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/core/service/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {

  Paymentform = new FormGroup({
    cardNumber: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    provider: new FormControl('', Validators.required),
    expiryMM: new FormControl('', Validators.required),
    expiryYY: new FormControl('', Validators.required),
    nameOnCard: new FormControl('', Validators.required)

  })

  constructor(
    private cardService: AddCardService, private _snackBar: MatSnackBar,
    private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    const cardDetails = { ...this.Paymentform.value, userID: this.authService.getUserID() }
    console.log(this.Paymentform.value);
    this.cardService.createCard((cardDetails))
      .subscribe((data: any) => {
        this._snackBar.open(data.message || 'Created!!', 'close', { panelClass: ['bg-success'] });
        console.log(data);
        this.router.navigate(['/cart']);
      })
  }
}
