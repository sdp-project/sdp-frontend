import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AddAddressService } from './add-address.service';
import { AuthService } from 'src/app/core/service/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss']
})
export class AddAddressComponent implements OnInit {

  Addressform = new FormGroup({
    country: new FormControl('', Validators.required),
    address1: new FormControl('', Validators.required),
    address2: new FormControl('', Validators.required),
    // landmark:new FormControl ('',Validators.required),
    city: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    postalCode: new FormControl('', Validators.required),
  })

  constructor(
    private addressService: AddAddressService,
    private authService: AuthService, private _snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit() {

  }
  onSubmit() {
    const userData = { ...this.Addressform.value, userID: this.authService.getUserID() }
    this.addressService.createAddress((userData))
      .subscribe((data) => {
        this._snackBar.open(data.message || 'Created!!', 'close', { panelClass: ['bg-success'] });
        console.log(data)
        this.router.navigate(['/cart']);

      })
  }

}

