import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  public profile: any;

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.customerService.getUserDetails().subscribe((result) => {
      console.log(result.data);
      this.profile = result.data;
    });
  }

}
