import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../customer.service';

@Component({
  selector: 'app-address-display',
  templateUrl: './address-display.component.html',
  styleUrls: ['./address-display.component.scss']
})
export class AddressDisplayComponent implements OnInit {
  addresses;

  constructor(private customer: CustomerService) { }

  ngOnInit(): void {
    this.customer.getAddresses().subscribe((result) => {
      console.log(result.data);
      this.addresses = result.data;
    })
  }

}
