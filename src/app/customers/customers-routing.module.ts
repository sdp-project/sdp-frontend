import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from '../core/guard/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddAddressComponent } from './profile/add-address/add-address.component';
import { AddressDisplayComponent } from './profile/address-display/address-display.component';
import { AddCardComponent } from './profile/add-card/add-card.component';
import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [
  { path: '', component: ProfileComponent },
  { path: '', component: ProfileComponent, canActivate: [AuthGuard] },
  // { path: 'profile', component: ProfileComponent },
  { path: 'order', component: OrdersComponent },
  { path: '', component: DashboardComponent },
  { path: 'add-address', component: AddAddressComponent },
  { path: 'view-address', component: AddressDisplayComponent, canActivate: [AuthGuard] },
  { path: 'add-card', component: AddCardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule {}
