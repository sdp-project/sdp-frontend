import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../core/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  getAddresses(): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.get(`${environment.apiUrl}/address/${userID}`);
  }
  getCard(): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.get(`${environment.apiUrl}/card/${userID}`);
  }

  getUserDetails(): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.get(`${environment.apiUrl}/user/${userID}`);
  }

  placeOrder(data): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.post(`${environment.apiUrl}/order/${userID}`, { ...data });
  }
}
