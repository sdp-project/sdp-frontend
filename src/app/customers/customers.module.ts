import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomersRoutingModule } from './customers-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { CartComponent } from './cart/cart.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { AddAddressComponent } from './profile/add-address/add-address.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressDisplayComponent } from './profile/address-display/address-display.component';
import { AddCardComponent } from './profile/add-card/add-card.component';
import { MatRadioModule } from '@angular/material/radio';
import { OrdersComponent } from './orders/orders.component';
import { OrderItemComponent } from './orders/order-item/order-item.component';
import { ViewOrderComponent } from './orders/view-order/view-order.component';
import { MatIconModule } from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    ProfileComponent,
    CartComponent,
    DashboardComponent,
    AddAddressComponent,
    CartItemComponent,
    AddressDisplayComponent,
    AddCardComponent,
    OrdersComponent,
    OrderItemComponent,
    ViewOrderComponent
  ],
  imports: [CommonModule,
    CustomersRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatIconModule,
    MatProgressSpinnerModule
  ],
})
export class CustomersModule { }
