import { Component, OnInit } from '@angular/core';
import { CartItemModel } from 'src/app/models/cart-item-model';
import { MessengerService } from './messenger.service';
import { Product } from 'src/app/models/product';
import { CartService } from './cart.service';
import { CustomerService } from '../customer.service';
import { AuthService } from 'src/app/core/service/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public cartList = [];

  cartTotal = 0;
  addressList: any;
  cardList: any;
  selectedCard: any;
  selectedAddress: any;
  public isSpinner = true;



  constructor(
    private msg: MessengerService,
    private cartService: CartService,
    private authService: AuthService,
    private customerService: CustomerService,
    private router: Router,
    private _snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    console.log('there?');
    this.msg.getMsg().subscribe((product: any) => {
      console.log('there again?');
      console.log(typeof (product));
      console.log(product);
      this.addProductToCart(product);

    });


    this.getCartList();

    this.getAddressList();
    this.getCardList();
  }

  getCartList() {
    this.cartService.getCartItem().subscribe((result: any) => {
      this.cartList = result.data;
      this.isSpinner = false;
      this.cartList.forEach(element => {
        this.cartTotal = element.price + this.cartTotal;
      });
      // for (let i in this.cartList) {
      //   this.cartTotal = this.cartList[i].price + this.cartTotal;
      // }
    });

  }

  addProductToCart(product) {
    console.log(product);

    let productExists = false;

    for (const i in this.cartList) {
      if (this.cartList[i].product_id === product.id) {
        this.cartList[i].qty++;
        productExists = true;
        break;
      }
    }

    // if(!productExists){
    //   this.cartList.push({
    //     productId:product.id,
    //     productName: product.productTitle,
    //     qty: 1,
    //     price: product.MRP
    //   })
    // }
  }




  getAddressList() {
    this.customerService.getAddresses().subscribe((result) => {
      console.log(result.data);
      this.addressList = result.data;
    });
  }

  getCardList() {
    this.customerService.getCard().subscribe((result) => {
      console.log(result.data);
      this.cardList = result.data;
    });
  }

  placeOrder() {
    this.isSpinner = true;
    const data = {
      userID: this.authService.getUserID(),
      cardID: this.selectedCard.id,
      addressID: this.selectedAddress.id
    };
    console.log(data);
    // this._snackBar.open('msg', 'close', { panelClass: [`bg-${'danger'}`] });
    this.customerService.placeOrder(data).subscribe((result) => {
      console.log(result.data);
      const msg = result.message;
      const status = result.status === 'success' ? result.status : 'danger';
      this._snackBar.open(msg, 'close', { panelClass: [`bg-${status}`] });
      this.isSpinner = false;
      if (result.status === 'success') {
        this.router.navigate(['/order']);
      }
    });
  }

  setSpinner(condition: boolean) {
    if (!condition) {
      this.getCartList();

    } else {
      this.isSpinner = condition;
    }
  }

  // calcTotal(){
  //   console.log("cart list ", this.cartList);
  //     for(let i in this.cartList){
  //       this.cartTotal = this.cartList[i].price +this.cartTotal;
  //     }
  //     return this.cartTotal;
  // }

}

