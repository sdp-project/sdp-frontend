import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { CartItemModel } from 'src/app/models/cart-item-model';
import { CartService } from '../cart.service';
import { NavigationEnd, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit, OnDestroy {
  @Input() cartItem: any;
  @Output() isAPIcalled = new EventEmitter<boolean>();
  sub: any;

  constructor(private cartService: CartService, private router: Router, private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  updateItem(id, qty) {
    console.log('update item', id, qty);
    this.isAPIcalled.emit(true);
    this.cartService.updateCartItemQty(id, qty).subscribe((result: any) => {
      this._snackBar.open('Item updated', 'close', { panelClass: ['bg-success'] });
      console.log(result.data);
      this.isAPIcalled.emit(false);
      // this.router.navigate(['/cart']);
      // location.reload();
    });
  }


  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
