import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from 'src/app/models/product';
import { AuthService } from 'src/app/core/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {


  public cartSubject: BehaviorSubject<any>; // publishing the id and qty - add to cart
  public cart$: Observable<any>; // listener the id and qty - add to cart
  public cartQty = 0;
  public cartQtySubject: BehaviorSubject<any>; // publishing no.of items
  public cartQty$: Observable<any>; // listen in header

  constructor(private http: HttpClient, private authService: AuthService) {
    this.cartSubject = new BehaviorSubject<any>({ id: 0, qty: 0 });
    this.cart$ = this.cartSubject.asObservable();
    this.cartQtySubject = new BehaviorSubject<number>(0);
    this.cartQty$ = this.cartQtySubject.asObservable();
    this.updateCart();
  }

  ngOnInIt() { }

  updateCart() {
    this.cartQty = 0;
    this.cart$.subscribe(data => {
      console.log('hey', data, this.cartQty);
      this.cartQty = this.cartQty + data.qty;
      this.cartQtySubject.next(this.cartQty);
    });
  }

  // fetching cart items for user
  getCartItem(): Observable<any> {
    const userID = this.authService.getUserID();
    return this.http.get(`${environment.apiUrl}/cart/${userID}`);
  }

  // adding data to cart with http call
  addProductToCart(cartItem: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/add-to-cart`, { ...cartItem });
  }

  updateCartItemQty(cartItemID, qty): Observable<any> {
    return this.http.put(`${environment.apiUrl}/cart/${cartItemID}/${qty}`, {});
  }
}
