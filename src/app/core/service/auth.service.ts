import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  redirectURL: string;


  private cartSubject: BehaviorSubject<any>;
  private cart$: Observable<any>;



  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.route.queryParams.subscribe(params => {
      this.redirectURL = params.returnUrl;
    });
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/login`, { email, password })
      .subscribe({
        next: (data) => {
          // console.log(data);
          const token = data.token;
          const decodedToken = this.jwtHelper.decodeToken(token);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(decodedToken));
          localStorage.setItem('id', JSON.stringify(decodedToken.id));
          localStorage.setItem('token', token);
          if (data.success) {
            this.redirect(data.message);
          }
          this.currentUserSubject.next((decodedToken));
          // return decodedToken;
        },
        error: (error) => {
          this._snackBar.open('Incorrect email or password', 'close', { panelClass: ['bg-danger'] });
          console.error('There was an error!', error);
        }
      });
  }

  redirect(msg) {
    console.log(this.redirectURL);
    const url = this.redirectURL ? this.redirectURL : '/product-list';
    this.router.navigate([url]);
    this._snackBar.open(msg || 'Logged in!!', 'close', { panelClass: ['bg-success'] });
  }

  // private setSession(authResult) {
  //   const expiresAt = moment().add(authResult.expiresIn, 'second');

  //   localStorage.setItem('id_token', authResult.idToken);
  //   localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
  // }

  validateUser() {
    const token = localStorage.getItem('token');
    const decodedToken = this.jwtHelper.decodeToken(token);
    const expirationDate = this.jwtHelper.getTokenExpirationDate(token);
    const isExpired = this.jwtHelper.isTokenExpired(token);
    // console.log(token, decodedToken, expirationDate, isExpired);
    return (expirationDate && !isExpired);
  }

  getUserID() {
    return localStorage.getItem('id');
  }

  logout() {
    // remove user from local storage to log user out
    console.log('user logged out, token cleared');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    localStorage.clear();
    this.router.navigate(['/login']);
    this.currentUserSubject.next(null);
  }
}
