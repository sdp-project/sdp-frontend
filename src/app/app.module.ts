import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SharedModule } from './shared/shared.module';

import { TestAreaComponent } from './test-area/test-area.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import {
  MatSnackBarModule, MatSnackBarConfig, MAT_SNACK_BAR_DEFAULT_OPTIONS,
} from '@angular/material/snack-bar';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { ProductsModule } from './products/products.module';
import { RegistrationComponent } from './registration/registration.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { MaterialsModule } from './materials/materials.module';
import { MatSliderModule } from '@angular/material/slider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRadioModule } from '@angular/material/radio';


import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ApiInterceptor } from './core/api.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
//import { AddCardComponent } from './profile/add-card/add-card.component';
//import { ProductPageComponent } from './pdoducts/product-page/product-page.component';


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [AppComponent, LoginComponent, TestAreaComponent, RegistrationComponent],
  imports: [
    CommonModule,
    BrowserModule,
    SharedModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    // MaterialsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: ['example.com', 'http://localhost:8080/'],
        blacklistedRoutes: ['http://example.com/examplebadroute/'],
      },
    }),
    ReactiveFormsModule,
    // module
    // Mat modules
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatSliderModule,
    MatGridListModule,
    MatRadioModule,
    MatButtonModule,
    MatSelectModule, MatCheckboxModule, MatDatepickerModule,
    MatSnackBarModule,
    MatIconModule,
    // ng-bootstrap
    // NgbRating,
    NgbModule,
    ProductsModule,
    // Keep app routing the last module to catch the wildcard paths
    AppRoutingModule,
    //ProductPageComponent
 
    MatSliderModule,
    MatGridListModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule
  ],
  providers: [{
    provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    }
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ApiInterceptor,
    multi: true,
  },],
  bootstrap: [AppComponent],
})
export class AppModule { }
