import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TestAreaComponent } from './test-area/test-area.component';
import { RegistrationComponent } from './registration/registration.component';
import { PLPPageComponent } from './products/plp-page/plp-page.component';
import { DashboardComponent } from 'src/app/customers/dashboard/dashboard.component';
import { PDPPageComponent } from 'src/app/products/pdp-page/pdp-page.component';
import { CartComponent } from './customers/cart/cart.component';
import { ProfileComponent } from './customers/profile/profile.component';
import { OrdersComponent } from './customers/orders/orders.component';
import { AuthGuard } from './core/guard/auth.guard';
import { NoAuthGuard } from './core/guard/no-auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'test', component: TestAreaComponent },
  { path: '', component: DashboardComponent },
  { path: 'order', component: OrdersComponent },
  { path: 'cart', component: CartComponent, canActivate: [AuthGuard] },
  // { path: 'products', component: PLPPageComponent },
  { path: 'profile', component: ProfileComponent },
  // { path: 'product/:id', component: PDPPageComponent },
  // { path: '', redirectTo: 'product' },
  { path: 'customers', loadChildren: () => import('./customers/customers.module').then((m) => m.CustomersModule) },
  { path: 'vendors', loadChildren: () => import('./vendors/vendors.module').then(m => m.VendorsModule) },
  {
    path: '**',
    redirectTo: 'product-list',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
