import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { RegistrationService } from './registration.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  form = new FormGroup({
    type: new FormControl('', Validators.required),
    salutation: new FormControl('', Validators.required),
    fName: new FormControl('', Validators.required),
    lName: new FormControl('', Validators.required),
    DOB: new FormControl('', Validators.required),
    gender: new FormControl('', Validators.required),
    // mobile: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
  })

  salutationList: string[] = ['Mr', 'Mrs', 'Ms'];

  constructor(private regService: RegistrationService, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  onSubmit() {
    this.regService.creatUserReg((this.form.value))
      .subscribe((data) => {

        console.log(data);
        if (data.status === 'success') {
          this.router.navigate(['/login']);
          this._snackBar.open('LOgged in!!', 'close');
        }
      });
  }

}
