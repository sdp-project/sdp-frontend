import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment}  from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class RegistrationService {

  constructor(private http: HttpClient) { }

  creatUserReg(value):Observable<any>{

      return this.http.post<any>(`${environment.apiUrl}/user`, { ...value })
      
  }

}
