export  class OrderModel{
    "id": number;
      "totalPrice": number;
      "orderStatus": string;
      "createdAt": string;
      "updatedAt": string;
      "userID": number;
      "addressID": number;
      "error": string;
      "paymentDate": string;
      "transactionStatus": string;
      "cardID": number;
      "uuid": string;

}