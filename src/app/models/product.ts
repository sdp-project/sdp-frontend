export class Product{
    id : number;
    productTitle :string;
    MRP : number;
    productDesc : string;
    rating : number;
    imgUrl: string;
    categoryID : number;
    qtySold : number;
    uuid:string;
    Brand:string;
    numberOfComment:string;
    weight:number;
    date:string;
    GTIN:string;

    // constructor(id,productTitle,MRP=0,productDesc,rating,productImg='src/assets/download.jpeg'){
    //     this.id = id
    //     this.productTitle= productTitle;
    //     this.productDesc = productDesc;
    //     this.MRP = MRP;
    //     this.rating = rating;
    //     this.productImg = productImg;
    // }
}