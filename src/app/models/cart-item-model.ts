export class CartItemModel {
     id : number;
     cartID:number;
     stockID:number;
     vendorID:number;
     qty : number;
    type : string;
    price : number;
    createdAt : Date;
    updatedAt  :Date;
}
