import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/service/auth.service';
import { CartService } from 'src/app/customers/cart/cart.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/products/products.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isUser = false;
  user = '';
  qty: number;
  searchItem = '';
  categoryList: any;


  constructor(
    private authService: AuthService,
    private cartService: CartService,
    private router: Router,
    private route: ActivatedRoute,
    private productsService: ProductsService
  ) {
    this.isUser = this.authService.validateUser();

    // this.user = this.authService.getUser();
    console.log(this.user)
    this.qty = 0;
    this.cartService.cartQty$.subscribe(data => {
      console.log('header cart:', data);
      this.qty = data;
    });
  }

  ngOnInit(): void {
    // this.user = this.authService.getUser();
    this.authService.currentUser.subscribe(data => {
      console.log(data);
      if (data) {
        this.user = data.email;
        this.isUser = true;
      } else {
        this.user = '';
        this.isUser = false;
      }
    });
    this.fetchCategories();
  }

  onEnter(data) {
    this.searchItem = data.trim();
    // if (this.searchItem.length > 1) {
    console.log(this.searchItem.length);
    console.log(this.searchItem);
    // this.searchItem="";
    this.router.navigate(['/product-list'],
      { relativeTo: this.route, queryParams: { search: this.searchItem }, queryParamsHandling: 'merge', });
    // }
  }

  fetchCategories() {
    this.productsService.getCatogories().subscribe((result: any) => {
      console.log(result.data);
      this.categoryList = result.data;
    })
  }







  logout() {
    this.authService.logout();
  }


}
