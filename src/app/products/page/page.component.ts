import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
// import { HttpClient } from '@angular/common/http';
import { ProductPageService } from './service.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  Productform = new FormGroup({
    ProductTitle: new FormControl('',Validators.required),
    description: new FormControl('',Validators.required),
    image: new FormControl('',Validators.required),
    MRP: new FormControl('',Validators.required),
    CategoryTitle: new FormControl('',Validators.required),
    qtySold: new FormControl('',Validators.required),
    weight : new FormControl('',Validators.required),
    GTIN: new FormControl ('',Validators.required)

  })

  selectedFile = null;
  ProductPageService: any;

  onFileSelected(event){
    console.log(event);
  }

  constructor(private productService: ProductPageService) {}

  ngOnInit() {
  }

  onSubmit() {
    this.productService.creatProduct((this.Productform.value))
    .subscribe((data)=>{

      console.log(data)
    })
  }


 
}
