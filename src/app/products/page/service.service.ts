import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment}  from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProductPageService {

  constructor(private http: HttpClient) { }

  creatProduct(value):Observable<any>{

      return this.http.post<any>(`${environment.apiUrl}/product`, { ...value })
      
  }
}