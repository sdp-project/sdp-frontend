import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { ProductsService } from 'src/app/products/products.service';
import { Product } from 'src/app/models/product';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'plp-page',
  templateUrl: './plp-page.component.html',
  styleUrls: ['./plp-page.component.scss']
})
export class PLPPageComponent implements OnInit, OnDestroy {
  productList: Product[] = [];
  private sub: any;
  searchObj: any;


  constructor(
    config: NgbRatingConfig,
    private products: ProductsService,
    private router: Router,
    private route: ActivatedRoute) {
    config.max = 5;
    // config.readonly = true;
  }

  ngOnInit(): void {
    this.sub = this.route.queryParams.subscribe(params => {
      this.searchObj = {
        search: params.search || '',
        order: params.order || 'DESC',
        filter: params.filter || 'MRP',
        start: params.start || '0',
        end: params.end || '10',
        categoryID: params.categoryID || ''
      };
      console.log(this.searchObj);

      this.getProductList(this.searchObj);
    });

  }

  getProductList(search) {
    this.products.getProducts(search).subscribe((result: any) => {
      console.log(result.data);
      this.productList = result.data;
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


  // rediredct(){
  //   this.router.navigate(['/product;id=72b9a03c-1d40-4cc4-8d49-203386d16bd0']);
  // }
}
