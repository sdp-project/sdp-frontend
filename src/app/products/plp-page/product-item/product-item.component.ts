import { Component, OnInit, Input } from '@angular/core';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { Product } from 'src/app/models/product';
import { ProductsService } from '../../products.service';
import { Router } from '@angular/router';

@Component({
  selector: 'product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() productItem: any;
  constructor(config: NgbRatingConfig, private productService: ProductsService, private router: Router) {
    config.max = 5;
    // config.readonly = true;

  }

  ngOnInit(): void {
  }

  onView() {
    console.log("view button clicked");
    // console.log(this.productItem);
    console.log(this.productItem.uuid, this.productItem.productTitle);
    //navigating to the url - adding url
    this.router.navigate(['/product', { id: this.productItem.uuid }]);

  }
}
