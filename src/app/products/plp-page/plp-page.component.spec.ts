import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PLPPageComponent } from './plp-page.component';

describe('PLPPageComponent', () => {
  let component: PLPPageComponent;
  let fixture: ComponentFixture<PLPPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PLPPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PLPPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
