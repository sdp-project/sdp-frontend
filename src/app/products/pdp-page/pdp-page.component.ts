import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CartService } from 'src/app/customers/cart/cart.service';
import { ProductsService } from '../products.service';
import { Product } from 'src/app/models/product';
import { MessengerService } from 'src/app/customers/cart/messenger.service';
import { CartItemModel } from 'src/app/models/cart-item-model';
import { AuthService } from 'src/app/core/service/auth.service';



@Component({
  selector: 'app-pdp-page',
  templateUrl: './pdp-page.component.html',
  styleUrls: ['./pdp-page.component.scss']
})
export class PDPPageComponent implements OnInit, OnDestroy {
  public id;
  private sub: any;
  quantity = [1, 2, 3];
  product: any;
  public vendors;
  public selectedVendor;
  public selectedQty = 1;
  cartItem: CartItemModel;
  public isLoggedin = false;
  counter = Array;
  isUser: boolean;

  constructor(
    private route: ActivatedRoute,
    config: NgbRatingConfig,
    private cartService: CartService,
    private productService: ProductsService,
    private msg: MessengerService,
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
    config.max = 5;
    // config.readonly = true;
    this.isLoggedin = (this.authService.getUserID()) ? true : false;
    console.log(this.isLoggedin);
    // this.authService.currentUser.subscribe(data => {
    //   console.log(data);
    //   if (data && this.authService.getUserID()) {
    //     this.isUser = true;
    //   } else {
    //     this.isUser = false;
    //   }
    // });
  }


  ngOnInit(): void {

    this.sub = this.route.params.subscribe(params => {
      // capturing id from url
      this.id = params.id; // (+) converts string 'id' to a number
      this.productService.getOneProduct(this.id).subscribe((result: any) => {
        this.product = result.product[0];
        this.vendors = result.vendorList;
        console.log(this.product);
        console.log(this.vendors);
      });
    });


    // //capturing queryparam from url
    // this.route.queryParams.subscribe(params => {
    //   // Defaults to 0 if no query param provided.
    //   // this.id = params['id'] || 0;
    //   console.log(params['id']);
    //   // this.productService.getOneProduct(this.id);

    // });
  }

  addCart() {
    console.log('pdp click');
    this.cartService.cartSubject.next({ id: 1, qty: 1 });
  }

  handleAddToCart() {
    const obj = {
      userID: this.authService.getUserID(),
      stockID: this.selectedVendor.stockID,
      vendorID: this.selectedVendor.vendorID,
      quantity: this.selectedQty
    };
    console.log(obj)
    this.cartService.addProductToCart(obj).subscribe((result) => {
      console.log(result);
      const status = result.status === 'success' ? result.status : 'danger';
      this._snackBar.open(result.message, 'close', { panelClass: [`bg-${status}`] });
      // this.msg.sendMsg(this.product);
    });
  }



  onvendorChange(ob) {
    console.log('vendor changed...');
    this.selectedVendor = ob.value;
    console.log(this.selectedVendor);
  }

  onQtyChange(ob) {
    console.log('qty changed...');
    this.selectedQty = ob.value;
    console.log(this.selectedQty);
  }

  numberReturn(length) {
    return new Array(length);
  }


  redirectToLogin() {
    this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url } });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
