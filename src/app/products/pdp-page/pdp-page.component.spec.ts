import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDPPageComponent } from './pdp-page.component';

describe('PDPPageComponent', () => {
  let component: PDPPageComponent;
  let fixture: ComponentFixture<PDPPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDPPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDPPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
