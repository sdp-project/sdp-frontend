import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PDPPageComponent } from './pdp-page/pdp-page.component';
import { NoAuthGuard } from '../core/guard/no-auth.guard';
import { PLPPageComponent } from './plp-page/plp-page.component';
import { PageComponent } from './page/page.component';

const routes: Routes = [
  { path: 'product-list', component: PLPPageComponent, canActivate: [NoAuthGuard] },
  { path: 'product', component: PDPPageComponent, canActivate: [NoAuthGuard] },
  { path: 'product/new', component: PageComponent, canActivate: [NoAuthGuard] },
];

// const routes: Routes = [
//  // { path: 'products', component: PLPPageComponent, canActivate: [NoAuthGuard] },
//   { path: 'product', component: PDPPageComponent }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
