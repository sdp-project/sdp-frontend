import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { ProductsRoutingModule } from './products-routing.module';
import { PDPPageComponent } from './pdp-page/pdp-page.component';
import { PLPPageComponent } from './plp-page/plp-page.component';
import { PageComponent } from './page/page.component';
import { ProductItemComponent } from './plp-page/product-item/product-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiltersComponent } from 'src/app/products/filters/filters.component';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [PDPPageComponent, PLPPageComponent, PageComponent, ProductItemComponent,FiltersComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule
  ]
})
export class ProductsModule { }
