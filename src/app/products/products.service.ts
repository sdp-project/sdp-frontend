import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from 'src/app/models/product';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: Product[] = [];
  url = `${environment.apiUrl}/product`;
  searchResult = '';
  productsUrl = '/assets/products.json';

  constructor(private http: HttpClient) { }


  // //fetching all products
  // getProducts():Observable<Product[]>{
  //   return this.http.get<Product[]>(this.productsUrl);
  // }

  // fetching all products

  getProducts(searchObj):
    Observable<any[]> {
    const data = {
      name: searchObj.search,
      filter: searchObj.filter,
      order: searchObj.order,
      start: searchObj.start,
      end: searchObj.end,
      categoryID: searchObj.categoryID
    };
    return this.http.get<any[]>(this.url, { params: data });
  }


  // fetching single product details
  getOneProduct(id): Observable<Product> {
    return this.http
      .get<Product>(`${environment.apiUrl}/product/${id}`);
  }

  // fetching products based on category
  getCatogories(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/category`);
  }

  // http://localhost:8080/v1/product/f5664f47-6303-4acf-a065-382ae638e38b

  //   searchProducts(term: string): Observable<any> {
  //     if (term === '') {
  //       console.log('enter value');
  //     }
  //     else {
  //       const params = { q: term };
  //       return this.http.get(this.url, { params }).pipe(
  //         map(response => {
  //           console.log(response);
  //           return this.searchResult = response.items;
  //         })
  //       );
  //     }
  //   }

  //   // return the response
  //   public _searchProducts(term) {
  //     return this.searchProducts(term);
  //   }
}
