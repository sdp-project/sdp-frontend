import { Component, OnInit } from '@angular/core';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-test-area',
  templateUrl: './test-area.component.html',
  styleUrls: ['./test-area.component.scss']
})
export class TestAreaComponent implements OnInit {

  rating = 5;
  active = 1;

  constructor(ratingConfig: NgbRatingConfig) {
    ratingConfig.max = 6;
    ratingConfig.readonly = false;
  }

  ngOnInit(): void {
  }

}
